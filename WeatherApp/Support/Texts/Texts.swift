import UIKit

class Texts {
   static func text(_ text: Project) -> String {
      return text.rawValue
   }
   enum Title: String, CaseIterable {
      
      case empty        = ""
      case error        = "Ошибка"
      case noNetwork    = "No internet"
      case noMessage    = "No message"
      case sendEmail    = "Sent by"
      case save         = "Saved by"
      case registration = "Registration"
      case information  = "Информация"
      case install      = "Install"
      case addPhoto     = "Change photo"
      case purchases    = "Purchases"
      case signOut      = "Sign out"
      
      private func localizedString() -> String {
         return NSLocalizedString(self.rawValue, comment: "")
      }
      
      func getTextFor(title: Title?) -> String {
         return title!.localizedString()
      }
   }
   enum Message: String, CaseIterable {
      
      case empty                   = ""
      case noNetwork               = "Отсутствует подключение к сети, подключите Ваше устройство и попробуйте снова"
      case noJSON                  = "Извините произошла ошибка получения данных"
      case noMessage               = "У Вас нет не одного сообщения"
      case emptyField              = "Введите корректные данные и попробуйте снова"
      case cameraAccess            = "Чтоб использовать QR код, приложению нужен доступ к камере, перейдите в настройки и включите доступ"
      case exchangeResultDataEmpty = "Данные для отображения отсутствуют"
      case errorUnknown            = "Неизвестная ошибка"
      case noAccessContact         = "No access to your contacts, go to settings"
      case isFailureLocation       = "Вы запретили использование вашей геопозиции"
      
      private func localizedString() -> String {
         return NSLocalizedString(self.rawValue, comment: "")
      }
      
      func getTextFor(message: Message?) -> String {
         return message!.localizedString()
      }
   }
   enum MessageCustom {
      case message(String)
   }
   enum Worning {
      case non
   }
   
   enum Project: String {
      
      case empty
      case exchangeOffer = "Please note that by using zam.io services you represent and warrant that you do not reside in and are not a citizen of a restricted jurisdiction, and no laws and regulations of your country in any way prohibit or restrict using our services via investments in zam.io."
      
      private func localizedString() -> String {
         return NSLocalizedString(self.rawValue, comment: "")
      }
      func getTextFor(_ text: Project?) -> String {
         return text!.localizedString()
      }
   }
}
enum TextType {
   
   case Title
   case Message
   case Project
}
enum TextAlert: String, CaseIterable {
   
   case empty
   case noJSON
   case noNetwork
   
}
enum TextSimple {
   case some
}



