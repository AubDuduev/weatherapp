import AnimatedGradientView
import UIKit

final class TabBarView: UIView {
   
   public var actions: Closure<Int>!
   
   //MARK: - Outlets
   @IBOutlet weak var commonStackView: UIStackView!
   @IBOutlet weak var gradientView   : UIView!
   
   //MARK: - Array outlets
   @IBOutlet var iconsImageViews  : [UIImageView]!
   @IBOutlet var iconsViews       : [UIView]!
   @IBOutlet var titleLabels      : [UILabel]!
   @IBOutlet var buttonLabels     : [UIButton]!
   @IBOutlet var sectionStackViews: [UIStackView]!
   
   
   //MARK: - Outlets Constraint
   @IBOutlet weak var topCommonStackViewConstant   : NSLayoutConstraint!
   @IBOutlet weak var bottomCommonStackViewConstant: NSLayoutConstraint!
   
   @IBOutlet var heightLabelsConstant           : [NSLayoutConstraint]!
   
   public func setup(bottomPadding: CGFloat){
      //Stack view
      self.topCommonStackViewConstant.constant = 15
      self.sectionStackViews.forEach { $0.spacing = 5 }
      //Labels
      self.titleLabels.forEach { $0.text      = TabBarText.allCases[$0.tag].rawValue }
      self.titleLabels.forEach { $0.font      = .set(.mavenProRegular, 10) }
      self.titleLabels.forEach { $0.textColor = .black }
      self.heightLabelsConstant.forEach { $0.constant = 14 }
      //Images
      self.iconsImageViews.forEach { $0.tintColor = .white}
      self.iconsImageViews.forEach { $0.image     = UIImage(named: TabBarImage.allCases[$0.tag].rawValue )}
      //Background
      self.iconsImageViews.forEach{ $0.backgroundColor = .white }
      self.titleLabels.forEach    { $0.backgroundColor = .white }
      self.iconsViews.forEach     { $0.backgroundColor = .white }
      self.backgroundColor = .white
      //padding
      self.bottomCommonStackViewConstant.constant = 25
      //
      //Images set color
      self.iconsImageViews.forEach { $0.alpha = 0.5 }
      self.iconsImageViews[0].alpha         = 1
      //Label set color
      self.titleLabels.forEach { $0.alpha = 0.5 }
      self.titleLabels[0].alpha           = 1
   }
   private func animationClick(tag: Int){
      UIView.animate(withDuration: 1,
                     delay: 0,
                     usingSpringWithDamping: 0.5,
                     initialSpringVelocity : 1.0,
                     options: .curveEaseInOut, animations: { [unowned self] in
                        //Images set color
                        self.iconsImageViews.forEach { $0.alpha = 0.5 }
                        self.iconsImageViews[tag].alpha         = 1
                        //Label set color
                        self.titleLabels.forEach { $0.alpha = 0.5 }
                        self.titleLabels[tag].alpha         = 1
                     })
   }
   private func testGeoposition(index: Int) -> Bool {
      guard GDInternetСheck.check() else { return false}
      if index == 1 && !GDLocationManager.shared.isState() {
         GDLocationManager.shared.authorisation()
         return false
      } else {
         return true
      }
   }
   @IBAction func actionButton(button: UIButton){
      guard self.testGeoposition(index: button.tag) else { return }
      self.actions?(button.tag)
      self.animationClick(tag: button.tag)
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
   }
}

enum TabBarImage: String, CaseIterable {
   
   case currentWeatherTabBar
   case forecastTabBar
}

enum TabBarText: String, CaseIterable {
   
   case Main
   case Forecast
}

