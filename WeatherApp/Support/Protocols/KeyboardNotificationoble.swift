
import UIKit

protocol KeyboardNotificationoble: AnyObject {
  
  func keyboardWillShow(_ notification: Notification, show: Bool)
  func keyboardWillHide(_ notification: Notification, show: Bool)
  
}

extension UIViewController: KeyboardNotificationoble {
  
  public func setupNotification(){
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil)
    {(notification) in
      self.keyboardWillShow(notification, show: true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)
    {(notification) in
      self.keyboardWillHide(notification, show: false)
    }
  }
  func keyboardWillShow(_ notification: Notification, show: Bool) {
    
  }
  func keyboardWillHide(_ notification: Notification, show: Bool) {
    
  }
}
