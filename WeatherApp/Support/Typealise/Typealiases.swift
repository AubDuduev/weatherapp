import Foundation
//import FirebaseAuth
//import FirebaseFirestore

//MARK:- RESULTS CLOSURE
typealias ClosurePictureSession = ((PictureSessionResult) -> Void)
typealias ClosureServerResult   = ((ServerResult) -> Void)
typealias ClosureDecode         = ((DecodeResult) -> Void)
typealias ClosureSession        = ((SessionResult) -> Void)
typealias ClosureSessionData    = ((SessionDataResult) -> Void)
typealias ClosureRequest        = ((RequestResult) -> Void)
typealias ClosureResultFB       = ((FirebaseResult) -> Void)

//MARK: - CLOUSURES
typealias ClosureReturn<T>      = (() -> T)
typealias Closure<T>            = ((T) -> Void)
typealias ClosureEmpty          = (() -> Void)
typealias ClosureTwo<T, G>      = ((T, G) -> Void)
typealias ClosureAny            = ((Any?) -> Void)

//MARK: - CUSTOM TYPE
typealias Header    = [String: String]
typealias ReturnURL = (string: String?, URL: URL?)
//typealias ReturnFB  = (url: CollectionReference?, doc: DocumentReference?)
