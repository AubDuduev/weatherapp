//
//  UserDataManager.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//
import CoreLocation
import Foundation

class UserDataManager {
   
   static let shared = UserDataManager()
   
   //MARK: - Private
   private let coreData = CoreData()
   private let group    = DispatchGroup()
   //MARK: - Public
   public var userData      : CDUserData!
   public var currentWeather: CDCurrentWeather!
   
   public func update(completion: @escaping ClosureEmpty){
      self.group.enter()
      self.coreData.fetch(object: CDUserData.self) { [weak self] userData in
         guard let self = self else { return }
         if let userData = userData as? CDUserData {
            self.userData = userData
            PersistentService.saveContext()
            self.group.leave()
         } else {
            
            self.userData = CDUserData()
            PersistentService.saveContext()
            self.group.leave()
         }
      }
      self.group.enter()
      self.coreData.fetch(object: CDCurrentWeather.self) { [weak self] currentWeather in
         guard let self = self else { return }
         if let currentWeather = currentWeather as? CDCurrentWeather {
            self.currentWeather = currentWeather
            PersistentService.saveContext()
            self.group.leave()
         } else {
            
            self.currentWeather = CDCurrentWeather()
            PersistentService.saveContext()
            self.group.leave()
         }
      }
      self.group.notify(queue: .main) {
         completion()
      }
   }
   public func updateUserDataLocation(coordinate2D: CLLocationCoordinate2D){
      self.userData.latitude  = coordinate2D.latitude
      self.userData.longitude = coordinate2D.longitude
      PersistentService.saveContext()
   }
   public func updateCurrentWeather(currentWeather: DECCurrentWeather){
      self.currentWeather.wind        = String(currentWeather.wind.speed ?? 0.0)
      self.currentWeather.city        = currentWeather.name
      self.currentWeather.temperature = String(currentWeather.weatherData?.temp ?? 0.0)
      self.currentWeather.descriptor  = currentWeather.weathers?.first?.description
      self.currentWeather.weatherIcon = currentWeather.weathers?.first?.weatherType.rawValue ?? ""
      self.userData.city              = currentWeather.name
      PersistentService.saveContext()
   }
}
