//
//  DayParts.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//
import Foundation

class DayParts {
   
   var currentHour:Int!
   var localLang  :String?
   
   // IDEA: Build a .plist or a REST API service or whatever that simply returns a dictiontary
   enum LetterCodes: String {
      
      case morning
      case day
      case evening
      case night
      
      init(hour: Int) {
         switch true {
            case (hour > 5 && hour < 12):
               self.init(rawValue: "morning")!
            case (hour > 12 && hour < 16):
               self.init(rawValue: "day")!
            case (hour > 16 && hour < 21):
               self.init(rawValue: "evening")!
            case (hour > 21 && hour < 5):
               self.init(rawValue: "night")!
            default:
               self.init(rawValue: "night")!
         }
      }
   }
   
   public func getLocateHour(){
      //A. Get the current time
      let date = Date()
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "HH"
      //B. Get the current hour
      currentHour = Int(dateFormatter.string(from: date))!
      //C. Get the current phone language
      localLang = NSLocale.current.localizedString(for: .gregorian)
   }
   func get(hour: Int) -> LetterCodes {
      return LetterCodes(hour: hour)
   }
   func now() -> LetterCodes {
      self.getLocateHour()
      return LetterCodes(hour: self.currentHour)
   }
}
