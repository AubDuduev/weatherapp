//
//  GDActivityIndicator.swift
//  wallet
//
//  Created by Senior Developer on 03.06.2021.
//  Copyright © 2021 zamzam. All rights reserved.
//
import NVActivityIndicatorView
import UIKit
import SnapKit

class GDActivityIndicator {
   
   private var blurView: UIView = {
      $0.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
      return $0
   }(UIView(frame: UIScreen.main.bounds))
   
   lazy private var loaderWindow: UIWindow = {
      $0.rootViewController = UIViewController()
      $0.windowLevel = .normal + 1
      $0.makeKeyAndVisible()
      let center =  UIApplication.shared.windows.first!.center
      $0.frame   = UIScreen.main.bounds
      $0.center  = center
      return $0
   }(UIWindow(windowScene: UIApplication.shared.windows.first!.windowScene!))
   
   private var loading    : NVActivityIndicatorView!
   private var viewLoading: UIView!
   //MARK: -
   public func fullLoading(color: UIColor = .clear, padding: CGFloat = 150, type: NVActivityIndicatorType = .circleStrokeSpin){
      let tabBarVC = UIApplication.shared.windows.first!.rootViewController as! MainTabBarViewController
      let tabBarView = tabBarVC.tabBarView
      let frame = loaderWindow.rootViewController!.view.frame
      self.loaderWindow.rootViewController!.view.backgroundColor = #colorLiteral(red: 0.03900792077, green: 0.07676046342, blue: 0.3283217251, alpha: 0.5)
      self.loaderWindow.sendSubviewToBack(tabBarView)
      self.loaderWindow.rootViewController!.view.alpha = 0.5
      self.loading = NVActivityIndicatorView(frame  : frame,
                                             type   : type,
                                             color  : color,
                                             padding: 0)
      self.loaderWindow.rootViewController?.view.addSubview(loading)
      self.constraintsFull(padding: padding)
      self.addGesture(view: self.loading)
   }
   //MARK: -
   public func localSetup(view: UIView, padding: CGFloat = 0, color: UIColor = .clear, type: NVActivityIndicatorType = .circleStrokeSpin){
      self.loading = NVActivityIndicatorView(frame  : view.frame,
                                             type   : type,
                                             color  : color,
                                             padding: 0)
      self.viewLoading = view
      self.viewLoading.addSubview(self.loading)
      self.constraintsLocal(padding: padding)
   }
   //MARK: -
   public func isAnimationLocal(show: Bool){
      if show {
         self.loading.startAnimating()
      } else {
         self.loading.stopAnimating()
      }
   }
   //MARK: -
   public func isAnimationFull(show: Bool){
      if show {
         self.loading.startAnimating()
         UIView.animate(withDuration: 0.3) {
            self.loaderWindow.alpha = 1
         }
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.4){
            self.loaderWindow.isHidden = false
         }
      } else {
         self.loading.stopAnimating()
         UIView.animate(withDuration: 0.3) {
            self.loaderWindow.alpha = 0
         }
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.4){
            self.loaderWindow.isHidden = true
         }
      }
   }
   //MARK: - Constraints
   private func constraintsFull(padding: CGFloat = 0){
      self.loading.snp.makeConstraints { [weak self] make in
         guard let self = self else { return }
         make.left.equalTo(self.loaderWindow.rootViewController!.view).offset(padding)
         make.right.equalTo(self.loaderWindow.rootViewController!.view).offset(-padding)
         make.bottom.equalTo(self.loaderWindow.rootViewController!.view).offset(-padding)
         make.top.equalTo(self.loaderWindow.rootViewController!.view).offset(padding)
      }
   }
   //MARK: - Constraints
   private func constraintsLocal(padding: CGFloat = 0){
      self.loading.snp.makeConstraints { [weak self] make in
         guard let self = self else { return }
         make.left.equalTo(self.viewLoading).offset(padding)
         make.right.equalTo(self.viewLoading).offset(-padding)
         make.bottom.equalTo(self.viewLoading).offset(-padding)
         make.top.equalTo(self.viewLoading).offset(padding)
      }
   }
   //MARK: -
   private func addGesture(view: UIView){
      let selector = #selector(gesture)
      let gesture = UIGestureRecognizer(target: self, action: selector)
      view.addGestureRecognizer(gesture)
      view.isUserInteractionEnabled = true
   }
   @objc private func gesture(){
      let mainTabBarVC = UIStoryboard.createVCID(sbID: .MainTabBar, vcID: .MainTabBarVC)
      UIApplication.shared.windows.first?.rootViewController = mainTabBarVC
      UIApplication.shared.windows.first?.makeKeyAndVisible()
   }
}
