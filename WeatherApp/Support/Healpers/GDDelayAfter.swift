import Foundation

class DelayAfter {
   
   static let shared = DelayAfter()
   private let queue = OperationQueue()
   
   private var execute: ClosureEmpty!
   
   private func deleteAfter(seconds: Double) {
      self.cancelDelete()
      let date = Date(timeIntervalSinceNow: seconds)
      self.queue.schedule(after: .init(date)) { [weak self] in
         guard let self = self else { return }
         self.execute()
      }
   }
   private func cancelDelete() {
      queue.cancelAllOperations()
   }
   public func time(seconds: Double, completion: @escaping ClosureEmpty){
      //1 - Отмена запрос на поиск
      self.cancelDelete()
      self.deleteAfter(seconds: seconds)
      
      //3 - Запрос на сервер
      DelayAfter.shared.execute = {
         DispatchQueue.main.async {
            completion()
         }
      }
   }
   private init(){}
}

