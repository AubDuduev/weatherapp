//
//  NetworkCheck.swift
//  DgBetTrip
//
//  Created by Senior Developer on 19.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import UIKit

class OpenURL {
   
   static let shared = OpenURL()
   
   public func openString(urlString: String?){
      guard let string = urlString else { return }
      guard let url = URL(string: string) else { return }
      guard UIApplication.shared.canOpenURL(url) else { return }
      UIApplication.shared.open((url), options: [:], completionHandler: nil)
   }
   public func callPhoneURL(number: PhoneNumbers){
      let urlNumber = "tel://\(number.rawValue)"
      guard let url = URL(string: urlNumber) else { return }
      guard UIApplication.shared.canOpenURL(url) else { return }
      UIApplication.shared.open((url), options: [:], completionHandler: nil)
   }
   public func openStringDecode(urlString: String?){
      guard let string = urlString else { return }
      guard let url = URL(string: string.decodedUrl()) else { return }
      guard UIApplication.shared.canOpenURL(url) else { return }
      UIApplication.shared.open((url), options: [:], completionHandler: nil)
   }
   public func openURLs(urls: URLStaticString.Url){
      guard let url = URL(string: urls.rawValue) else { return }
      guard UIApplication.shared.canOpenURL(url) else { return }
      UIApplication.shared.open((url), options: [:], completionHandler: nil)
   }
   public func openURL(url: URL?){
      guard let url = url else { return }
      guard UIApplication.shared.canOpenURL(url) else { return }
      UIApplication.shared.open((url), options: [:], completionHandler: nil)
   }
   public func openCompare(url: URL?, compareURL: CompareURL){
      if compareURL.rawValue == url?.absoluteString {
         guard let url = url else { return }
         guard UIApplication.shared.canOpenURL(url) else { return }
         UIApplication.shared.open((url), options: [:], completionHandler: nil)
      }
   }
   public func compare(urlString: String?, compareURL: CompareURL) -> Bool {
      guard let urlString = urlString else { return false }
      if compareURL.rawValue == urlString {
         return true
      } else {
         return false
      }
   }
   public func openReplaceURL(url: URL?, getURL: GetURL, postURL: PostURL){
      if getURL.rawValue == url?.absoluteString {
         guard let url = URL(string: postURL.rawValue) else { return }
         guard UIApplication.shared.canOpenURL(url) else { return }
         UIApplication.shared.open((url), options: [:], completionHandler: nil)
      }
   }
   public func openSetting(app: App){
      switch app {
         case .Setting:
            guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
            guard UIApplication.shared.canOpenURL(url) else { return }
            UIApplication.shared.open((url), options: [:], completionHandler: nil)
      }
   }
   enum CompareURL: String {
      case none = ""
   }
   enum PostURL: String {
      case none = ""
      
   }
   enum App {
      case Setting
   }
   enum PhoneNumbers: String {
      case none
   }
   enum GetURL: String {
      case none = ""
   }
   private init(){}
}
