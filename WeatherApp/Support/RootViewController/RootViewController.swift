
import UIKit

class RootVC {
  
  public func set(window: UIWindow?){
    let rootViewController = UIStoryboard.createVC(sbID: .MainTabBar)
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
  }
}

