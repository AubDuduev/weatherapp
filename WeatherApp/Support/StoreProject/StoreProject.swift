//
//  StoreProject.swift

import Foundation

class StoreProject: NSObject {
   
   public static let shared = StoreProject()
   
   private let standard = UserDefaults.standard
   
   public func save(key: StoreProjectKey.Key, value: Any?){
      if let value = value {
         standard.set(value, forKey: key.rawValue)
      } else {
         standard.set("", forKey: key.rawValue)
      }
   }
   
   public func get(key: StoreProjectKey.Key) -> Any {
      if let value = standard.object(forKey: key.rawValue) {
         return value
      } else {
         return false
      }
   }
   public func getBool(key: StoreProjectKey.Key = StoreProjectKey.Key.null, keys: StoreProjectKey.Keys = .null, keyString: String? = nil) -> Bool {
      if let string = keyString {
         return standard.bool(forKey: string)
      } else if key != .null {
         return standard.bool(forKey: key.rawValue)
      } else {
         switch keys {
            case .likesAndDislike(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.bool(forKey: key)
            case .likesAndDislikeInfoButton(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.bool(forKey: key)
            case .isOrder(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.bool(forKey: key)
            case .null:
               return false
         }
      }
   }
   public func saveBool(key: StoreProjectKey.Key = .null, keys: StoreProjectKey.Keys = .null, keyString: String? = nil, value: Bool){
      if let string = keyString {
         standard.set(value, forKey: string)
      } else if key != .null {
         standard.set(value, forKey: key.rawValue)
      } else {
         switch keys {
            case .likesAndDislike(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .likesAndDislikeInfoButton(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .isOrder(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .null:
               print("")
         }
      }
   }
   public func getString(key: StoreProjectKey.Key = StoreProjectKey.Key.null, keys: StoreProjectKey.Keys = .null, keyString: String? = nil) -> String {
      if let string = keyString {
         return standard.string(forKey: string) ?? ""
      } else if key != .null {
         return standard.string(forKey: key.rawValue) ?? ""
      } else {
         switch keys {
            case .likesAndDislike(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.string(forKey: key) ?? ""
            case .likesAndDislikeInfoButton(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.string(forKey: key) ?? ""
            case .isOrder(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.string(forKey: key) ?? ""
            case .null:
               return ""
         }
      }
   }
   public func saveString(key: StoreProjectKey.Key = .null, keys: StoreProjectKey.Keys = .null, keyString: String? = nil, value: String?){
      
      if let string = keyString {
         standard.set(value, forKey: string)
      } else if key != .null {
         standard.set(value, forKey: key.rawValue)
      } else {
         switch keys {
            case .likesAndDislike(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .likesAndDislikeInfoButton(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .isOrder(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .null:
               print("")
         }
      }
   }
   public func getInt(key: StoreProjectKey.Key = StoreProjectKey.Key.null, keys: StoreProjectKey.Keys = .null, keyString: String? = nil) -> Int? {
      if let string = keyString {
         return standard.integer(forKey: string)
      } else if key != .null {
         return standard.integer(forKey: key.rawValue)
      } else {
         switch keys {
            case .likesAndDislike(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.integer(forKey: key)
            case .likesAndDislikeInfoButton(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.integer(forKey: key)
            case .isOrder(let oneParam, let twoParam):
               let key = oneParam.rawValue + (twoParam ?? "")
               return standard.integer(forKey: key)
            case .null:
               return nil
         }
      }
   }
   public func saveInt(key: StoreProjectKey.Key = .null, keys: StoreProjectKey.Keys = .null, keyString: String? = nil, value: Int?){
      if let string = keyString {
         standard.set(value, forKey: string)
      } else if key != .null {
         standard.set(value, forKey: key.rawValue)
      } else {
         switch keys {
            case .likesAndDislike(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .likesAndDislikeInfoButton(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .isOrder(let keys, let twoParam):
               let key = keys.rawValue + (twoParam ?? "")
               standard.set(value, forKey: key)
            case .null:
               print("")
         }
      }
   }
   public func getDictionary(key: StoreProjectKey.Key) -> [String: String]? {
      return standard.dictionary(forKey: key.rawValue) as? [String: String]
   }
   public func saveDictionary(key: StoreProjectKey.Key, dictionary: [String: String]){
      standard.set(dictionary, forKey: key.rawValue)
   }
}

class StoreProjectKey {
   
   enum Keys {
      case likesAndDislike(Key, String?)
      case isOrder(Key, String?)
      case likesAndDislikeInfoButton(Key, String?)
      case null
   }
   enum Key: String {
      
      case null
      case token
      case isUserPersonalDataSave
      case tokenAccess
      case isByZamOfferSuccess
      case isByOtherOfferSuccess
      case phoneNumber = "phone_number"
   }
   
   private init(){}
}
