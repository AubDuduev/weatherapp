
import UIKit

extension UIApplication {
  
  static func mainVC(_ rootVC: UIViewController? = UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
    
    if let nav = rootVC as? UINavigationController {
      return mainVC(nav.visibleViewController)
    }
    if let tab = rootVC as? UITabBarController {
      if let selected = tab.selectedViewController {
        return mainVC(selected)
      }
    }
    if let presented = rootVC?.presentedViewController {
      return mainVC(presented)
    }
    
    return rootVC
  }
  
  enum Types {
    case LastWindowVC
    case RootVC
    case TabBarVC(Int)
    case LastChild
  }

  static private func presentLog(){
    print("-------------------------")
//    print(UIApplication.shared.keyWindow!.rootViewController! , "Window delegate rootViewController")
//    print(UIApplication.shared.keyWindow!.rootViewController! , "KeyWindow rootViewController")
//    print(UIApplication.shared.keyWindow! , "keyWindow")
//    UIApplication.shared.windows.forEach{ print($0.rootViewController!, "RootViewControllers")}
//    print(UIApplication.shared.windows.count, "Count Windows")
//    UIApplication.shared.windows.forEach{ print($0, "Windows")}
    print("-------------------------")
  }
  var currentScene: UIWindowScene? {
      connectedScenes
          .first { $0.activationState == .foregroundActive } as? UIWindowScene
  }
}


