
import UIKit

extension Notification.Name {
   
   static func id(_ id: Notification.Name.ID) -> Notification.Name {
      let name = Notification.Name(id.rawValue)
      return name
   }
   
   enum ID: String {
      case completed
      case notificationSusses
      case inputAmount
      case coinShortName
      case keyboardText
      case datePickerDone
      case datePickerDateChange
   }
}

extension Notification {
   
   var keyboardHeight: CGFloat {
      return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
   }
}
