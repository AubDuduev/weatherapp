
import Foundation

extension NumberFormatter {
   
   static var output: NumberFormatter {
      let formatter = NumberFormatter()
      formatter.locale = Locale(identifier: "en_US_POSIX")
      
      return formatter
   }
   
   static func amount(minimumFractionDigits: Int, maximumFractionDigits: Int) -> NumberFormatter {
      let formatter = NumberFormatter()
      formatter.locale                = Locale.current
      formatter.decimalSeparator      = Locale.current.decimalSeparator
      formatter.numberStyle           = .decimal
      formatter.roundingMode          = .up
      formatter.minimumFractionDigits = minimumFractionDigits
      formatter.maximumFractionDigits = maximumFractionDigits
      
      if minimumFractionDigits > 0 {
         formatter.alwaysShowsDecimalSeparator = true
      }
      return formatter
   }
   static func amount(minimum: Int, maximum: Int) -> NumberFormatter {
      let formatter = NumberFormatter()
      formatter.locale                = Locale(identifier: "ru_RUS")
      formatter.decimalSeparator      = Locale.current.decimalSeparator
      formatter.numberStyle           = .decimal
      formatter.roundingMode          = .floor
      formatter.minimumFractionDigits = minimum
      formatter.maximumFractionDigits = maximum
      //Убираем точку если после точки нет цифр
      if minimum > 0 {
         formatter.alwaysShowsDecimalSeparator = true
      }
      return formatter
   }
   
   static func amountDecimal(sum: inout String) -> Decimal {
      sum.removeFirst()
      let result = Decimal(string: sum)!
      return result
   }
   enum Direction {
      
      case forward
      case back
   }
   enum Symbols: String {
      
      case dollar = "$"
      case USD
      case BTC
      case ETH
      case ZAM
   }
}
