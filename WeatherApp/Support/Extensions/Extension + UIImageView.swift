
import UIKit

extension UIImageView {
   
   func set(nameImage: NameImage?){
      self.image = UIImage(named: nameImage?.rawValue ?? "")
   }
   
   enum NameImage: String {
      
      case eyeOpen
      case eyeClose
      case icArrowUpGreen
      case icArrowDownBlue
      case exchangeSuccess
      case exchangeFailure
      case bitcoinIcon
      case zamIcon
      case bitcoinCashIcon
      case ethereumIcon
      case checkmarkExchange
      //Main
      case mainIconExchange
      case mainIconZamZam
      case mainIconDeposit
      case mainIconSend
      case zamzamCoins
      case mainNewsOneImage
      //TabBar
      case exchangeTabBar
      case mainTabBar
      case moreTabBar
      case verifyTabBar
      case transactionsTabBar
      
      //Инициализатор для создания монеты
      init?(coinIcon: String?) {
         
         switch coinIcon {
            case "eth":
               self.init(rawValue: "ethereumIcon")
            case "btc":
               self.init(rawValue: "bitcoinIcon")
            case "zam":
               self.init(rawValue: "zamIcon")
            default:
               self.init(rawValue: "")
         }
      }
   }
   func qrCodeCreate(from string: String){
      let data = string.data(using: String.Encoding.ascii)
      
      if let filter = CIFilter(name: "CIQRCodeGenerator") {
         filter.setValue(data, forKey: "inputMessage")
         filter.setValue("Q", forKey: "inputCorrectionLevel")
         
         guard let output = filter.outputImage else { return }
         
         let result = UIImage(ciImage: output)
         
         let scaleX = self.bounds.width / result.size.width
         let scaleY = self.bounds.height / result.size.height
         
         guard let transformedImage = result.ciImage?.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY)) else { return }

         self.image = UIImage(ciImage: transformedImage)
      }
   }
}



