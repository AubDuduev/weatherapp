//
//  Extension + CLLocation.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//

import CoreLocation

extension CLLocation {
   func fetchAddress(completion: @escaping (_ address: String?, _ error: Error?) -> ()) {
      CLGeocoder().reverseGeocodeLocation(self) {
         let palcemark = $0?.first
         var address = ""
         if let subThoroughfare = palcemark?.subThoroughfare {
            address = address + subThoroughfare + ","
         }
         if let thoroughfare = palcemark?.thoroughfare {
            address = address + thoroughfare + ","
         }
         if let locality = palcemark?.locality {
            address = address + locality + ","
         }
         if let subLocality = palcemark?.subLocality {
            address = address + subLocality + ","
         }
         if let administrativeArea = palcemark?.administrativeArea {
            address = address + administrativeArea + ","
         }
         if let postalCode = palcemark?.postalCode {
            address = address + postalCode + ","
         }
         if let country = palcemark?.country {
            address = address + country + ","
         }
         if address.last == "," {
            address = String(address.dropLast())
         }
         completion(address,$1)
          completion("\($0?.first?.subThoroughfare ?? ""), \($0?.first?.thoroughfare ?? ""), \($0?.first?.locality ?? ""), \($0?.first?.subLocality ?? ""), \($0?.first?.administrativeArea ?? ""), \($0?.first?.postalCode ?? ""), \($0?.first?.country ?? "")",$1)
      }
   }
}
