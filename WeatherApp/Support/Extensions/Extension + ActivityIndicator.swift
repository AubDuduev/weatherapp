
import UIKit

extension UIActivityIndicatorView {
	
	func show(_ activity: Bool){
		if activity {
			self.startAnimating()
			self.isHidden = false
		} else {
			self.stopAnimating()
			self.isHidden = true
		}
	}
}
