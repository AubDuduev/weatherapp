import UIKit

extension UIScrollView {
   
   public func isBouncesBottom(to: CGFloat) -> Bool {
      let contentHeight  = self.contentSize.height
      let contentVisible = self.visibleSize.height
      let result = (contentHeight - contentVisible) - to
      if self.contentOffset.y >= result {
         return false
      } else {
         return true
      }
   }
   public func isBouncesTop(to: CGFloat) -> Bool {
      if self.contentOffset.y <= to {
         return false
      } else {
         return true
      }
   }
   public func isBottom(to: CGFloat) -> Bool {
      let contentHeight  = self.contentSize.height
      let contentVisible = self.visibleSize.height
      let result = (contentHeight - contentVisible) + to
      
      if self.contentOffset.y > result {
         return true
      } else {
         return false
      }
   }
   public func isTop(to: CGFloat) -> Bool {
      if self.contentOffset.y < -to {
         return true
      } else {
         return false
      }
   }
   
   public func topOffset() -> CGFloat {
      return self.contentOffset.y
   }
}
