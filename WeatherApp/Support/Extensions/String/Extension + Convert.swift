//
//  Extension + Convert.swift
//  wallet
//
//  Created by Senior Developer on 19.05.2021.
//  Copyright © 2021 zamzam. All rights reserved.
//

import Foundation

extension String {
   
   public func convertFromStringToDecimal() -> Decimal {
      let replace = self.replacingOccurrences(of: ",", with: ".")
      let result  = Decimal(string: replace) ?? 0
      return result
   }
//   //Конвертируем из строки в Decimal и потом в строку но округленную до выбранной цифры
//   public func convertInDecimalRoundString(min: Int, max: Int) -> String {
//      let decimal = Decimal(string: self) ?? 0.0
//      let round   = decimal.roundUp(min: min, max: max)
//      return String(round)
//   }
   //Конвертируем из строки в Decimal и потом в строку но округленную до выбранной цифры
   public func convertInDecimalRoundString(max: Int) -> String {
      let decimal = Decimal(string: self) ?? 0.0
      let round   = decimal.rounded(max, .up)
      let result  = NumberFormatter.amount(minimumFractionDigits: 1, maximumFractionDigits: max).string(from: round as NSNumber)!
      let replace = result.replacingOccurrences(of: ",", with: ".")
      let stringResult = replace
      return stringResult
   }
   //Конвертируем из строки в Double и потом в строку но округленную до выбранной цифры
   public func convertInDoubleRoundString(max: Int, replace: ReplaceDot) -> String {
      let replaceStart = self.replacingOccurrences(of: ",", with: ".")
      let decimal    = Double(replaceStart) ?? 0.0
      let round      = decimal.roundTo(max)
      let result     = NumberFormatter.amount(minimumFractionDigits: 1, maximumFractionDigits: max).string(from: round as NSNumber)!
      var replaceEnd = result
      switch replace {
         case .Dot:
            replaceEnd = result.replacingOccurrences(of: ",", with: ".")
         case .Comma:
            replaceEnd = result.replacingOccurrences(of: ".", with: ",")
      }
      let stringResult = replaceEnd
      return stringResult
   }
   //Конвертируем из строки в Decimal и c округленнием
   public func convertInDecimalRound(max: Int) -> Decimal {
      let decimal = Decimal(string: self) ?? 0.0
      let round   = decimal.rounded(max, .up)
      return round
   }
}

public enum ReplaceDot {
   case Dot
   case Comma
}
