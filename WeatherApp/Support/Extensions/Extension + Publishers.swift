
import SwiftUI
import Combine
import Foundation

extension Publishers {
   // 1.
   static var keyboardHeight: AnyPublisher<CGFloat, Never> {
      // 2.
      let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification)
         .map { $0.keyboardHeight }
      
      let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification)
         .map { _ in CGFloat(0) }
      
      // 3.
      return MergeMany(willShow, willHide)
         .eraseToAnyPublisher()
   }
   // 1.
   static var isKeyboard: AnyPublisher<isKeyboardState, Never> {
      // 2.
      let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification)
         .map { _ in isKeyboardState.Up }
      
      let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification)
         .map { _ in isKeyboardState.Down }
      
      // 3.
      return MergeMany(willShow, willHide)
         .eraseToAnyPublisher()
   }
   static var textTextField: AnyPublisher<String, Never> {
      
      let inputAmount = NotificationCenter.default.publisher(for: .id(.inputAmount))
      
      let textMessage = Publishers.Map(upstream: inputAmount) { (notification) -> String in
         guard let message = (notification.object as? String) else { return ""}
         return message
      }
      return MergeMany(textMessage).eraseToAnyPublisher()
   }
   static var textFieldReceiveValue: AnyPublisher<String, Never> {
      
      let inputAmount = NotificationCenter.default.publisher(for: UITextField.textDidChangeNotification)
      
      let textMessage = Publishers.Map(upstream: inputAmount) { (notification) -> String in
         guard let message = (notification.object as? String) else { return ""}
         return message
      }
      return MergeMany(textMessage).eraseToAnyPublisher()
   }
   static var coinShortName: AnyPublisher<String, Never> {
      
      let coinShortName = NotificationCenter.default.publisher(for: .id(.coinShortName))
      
      let textMessage = Publishers.Map(upstream: coinShortName) { (notification) -> String in
         guard let message = (notification.object as? String) else { return ""}
         return message
      }
      return MergeMany(textMessage).eraseToAnyPublisher()
   }
   
   static var keyboardText: AnyPublisher<String, Never> {
      
      let keyboardText = NotificationCenter.default.publisher(for: .id(.keyboardText))
      
      let textMessage = Publishers.Map(upstream: keyboardText) { (notification) -> String in
         guard let message = (notification.object as? String) else { return ""}
         return message
      }
      return MergeMany(textMessage).eraseToAnyPublisher()
   }
   static var datePickerDone: AnyPublisher<Bool, Never> {
      let done = NotificationCenter.default.publisher(for: .id(.datePickerDone)).map({ done in true })
      return MergeMany(done).eraseToAnyPublisher()
   }
   
   static var datePickerDateChange: AnyPublisher<Date?, Never> {
      let changeDate = NotificationCenter.default.publisher(for: .id(.datePickerDateChange))
      let date = Publishers.Map(upstream: changeDate) { (notification) -> Date? in
         guard let date = (notification.object as? Date) else { return nil }
         return date
      }
      return MergeMany(date).eraseToAnyPublisher()
   }
}

enum isKeyboardState {
   
   case Up
   case Down
}
