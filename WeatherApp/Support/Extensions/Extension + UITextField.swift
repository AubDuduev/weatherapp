
import UIKit

extension UITextField {
	
	func setImageLeft(_ imageName: UIImageView.NameImage){
    let width  = self.frame.height - 10
    let height = self.frame.height - 10
		let imageView       = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
    imageView.image     = UIImage.set(.eyeOpen)
    let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
    view.addSubview(imageView)
    self.leftView       = view
		self.leftViewMode   = .always
	}
  func setImageRight(_ imageName: UIImageView.NameImage){
//    let width  = self.frame.height - 10
//    let height = self.frame.height - 10
//    let imageView = UIImageView()
//    imageView.image = UIImage.set(.eyeOpen)
//    imageView.tintColor = .red
//    imageView.frame = CGRect(origin: 0, size: 0)
//    self.rightViewMode = .always
//    self.rightView = UIView(frame: CGRect(x: 2.5, y: 5, width: width, height: height))
//    self.rightView!.addSubview(imageView)
    
  }
	func check() -> Bool {
		if self.text != nil, self.text != "" {
			return true
		}
		return false
	}
  public func compare(field: UITextField) -> Bool {
    if let oneText = self.text, let twoText = field.text, oneText == twoText {
      return true
    } else {
      return false
    }
  }
}

class ChangeTextRect: UITextField {
	
	override func textRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.insetBy(dx: 50, dy: 0)
	}
	override func editingRect(forBounds bounds: CGRect) -> CGRect {
		return self.textRect(forBounds: bounds)
	}
}
