//
//  UserData+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//
//

import Foundation
import CoreData


extension CDUserData {
   
   @nonobjc public class func fetchRequest() -> NSFetchRequest<CDUserData> {
      return NSFetchRequest<CDUserData>(entityName: "CDUserData")
   }
   
   @NSManaged public var latitude : Double
   @NSManaged public var longitude: Double
   @NSManaged public var city     : String?
}

extension CDUserData : Identifiable {
   
}
