//
//  UserData+CoreDataClass.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//
//

import Foundation
import CoreData

@objc(CDUserData)
public class CDUserData: NSManagedObject {
   
   
   public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
      super.init(entity: entity, insertInto: context)
   }
   
   convenience init() {
      self.init(context:  PersistentService.context)
      self.latitude  = 0.0
      self.longitude = 0.0
      self.city      = ""
   }

}
