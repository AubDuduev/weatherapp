//
//  CDCurrentWeather+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Senior Developer on 17.07.2021.
//
//

import Foundation
import CoreData


extension CDCurrentWeather {
   
   @nonobjc public class func fetchRequest() -> NSFetchRequest<CDCurrentWeather> {
      return NSFetchRequest<CDCurrentWeather>(entityName: "CDCurrentWeather")
   }
   
   @NSManaged public var temperature: String?
   @NSManaged public var wind       : String?
   @NSManaged public var descriptor : String?
   @NSManaged public var city       : String?
   @NSManaged public var weatherIcon: String?
   
}

extension CDCurrentWeather : Identifiable {
   
}
