//
//  CDCurrentWeather+CoreDataClass.swift
//  WeatherApp
//
//  Created by Senior Developer on 17.07.2021.
//
//

import Foundation
import CoreData

@objc(CDCurrentWeather)
public class CDCurrentWeather: NSManagedObject {
   
   
   public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
      super.init(entity: entity, insertInto: context)
   }
   
   convenience init() {
      self.init(context:  PersistentService.context)
      self.temperature = ""
      self.wind        = ""
      self.descriptor  = ""
      self.city        = ""
      self.weatherIcon = ""
      
   }
}
