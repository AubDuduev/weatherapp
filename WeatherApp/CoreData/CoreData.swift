
import UIKit
import CoreData

class CoreData {
   
   public func fetchName<T: NSManagedObject>(object: T, key: CoreDataKey, name: String, execute: @escaping Closure<T?>){
      
      let fetchRequest: NSFetchRequest = T.fetchRequest()
      fetchRequest.predicate = NSPredicate(format: "%@==\(key.rawValue)", name)
      do {
         let result = try PersistentService.context.fetch(fetchRequest).first as? T
         execute(result)
      } catch let error {
         print(error.localizedDescription, "Ошибка получения по имени объекта из CoreData")
      }
   }
   public func fetch<T: NSManagedObject>(object: T.Type,execute: @escaping ClosureAny){
      do {
         let result = try PersistentService.context.fetch(T.fetchRequest())
         execute(result.first)
      } catch let error {
         print(error.localizedDescription, "Ошибка получения массив объектов данного типа из CoreData")
         execute(nil)
      }
   }
   public func fetchArray<T: NSManagedObject>(object: T.Type,execute: @escaping Closure<[T]>){
      do {
         let result = try PersistentService.context.fetch(T.fetchRequest()) as! [T]
         execute(result)
      } catch let error {
         print(error.localizedDescription, "Ошибка получения массив объектов данного типа из CoreData")
      }
   }
   public func create<T: NSManagedObject>(object: T.Type) -> T {
      let object = T(context: PersistentService.context)
      PersistentService.saveContext()
      return object
   }
   
   public func delete(object: NSManagedObject?){
      guard let object = object else { return }
      PersistentService.context.delete(object)
      PersistentService.saveContext()
   }
   public func edit(){
      PersistentService.saveContext()
   }
   enum CoreDataKey: String, CaseIterable {
      case productID
   }
}
