
import Foundation

struct DECClouds {
  
   let all: Int!
  
  enum CodingKeys: String, CodingKey {
   
    case all
  }
}
extension DECClouds: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
   
    self.all = try? values.decode(Int?.self, forKey: .all)
  }
}
extension DECClouds: Hashable {
   
   static func == (lhs: DECClouds, rhs: DECClouds) -> Bool {
      return (lhs.all == rhs.all)
   }
}
