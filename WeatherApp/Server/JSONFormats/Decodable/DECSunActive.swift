
import Foundation

struct DECSunActive {
   
   let type   : Double?
   let id     : Double?
   let country: String?
   let sunrise: Double?
   let sunset : Double?
   
   enum CodingKeys: String, CodingKey {
      
      case type
      case id
      case country
      case sunrise
      case sunset
   }
}
extension DECSunActive: Decodable {
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      
      self.type    = try? values.decode(Double?.self, forKey: .type   )
      self.id      = try? values.decode(Double?.self, forKey: .id     )
      self.country = try? values.decode(String?.self, forKey: .country)
      self.sunrise = try? values.decode(Double?.self, forKey: .sunrise)
      self.sunset  = try? values.decode(Double?.self, forKey: .sunset )
   }
}
extension DECSunActive: Hashable {
   
   static func == (lhs: DECSunActive, rhs: DECSunActive) -> Bool {
      return (lhs.id == rhs.id)
   }
}
