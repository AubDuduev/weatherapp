
import Foundation

struct DECCoordinate {
   
   let longitude: Double!
   let latitude : Double!
   
   enum CodingKeys: String, CodingKey {
      
      case longitude = "lon"
      case latitude  = "lat"
   }
}
extension DECCoordinate: Decodable {
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      
      self.longitude = try? values.decode(Double?.self, forKey: .longitude)
      self.latitude  = try? values.decode(Double?.self, forKey: .latitude)
   }
}

extension DECCoordinate: Hashable {

   static func == (lhs: DECCoordinate, rhs: DECCoordinate) -> Bool {
      return (lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude)
   }
}
