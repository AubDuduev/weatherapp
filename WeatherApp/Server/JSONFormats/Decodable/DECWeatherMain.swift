
import Foundation

struct DECWeatherData {
   
   let temp     : Double!
   let feelsLike: Double!
   let tempMin  : Double!
   let tempMax  : Double!
   let pressure : Double!
   let humidity : Double!
   let seaLevel : Double!
   let grndLevel: Double!
   
   enum CodingKeys: String, CodingKey {
      
      case temp      = "temp"
      case feelsLike = "feels_like"
      case tempMin   = "temp_min"
      case tempMax   = "temp_max"
      case pressure  = "pressure"
      case humidity  = "humidity"
      case seaLevel  = "sea_level"
      case grndLevel = "grnd_level"
   }
}
extension DECWeatherData: Decodable {
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      
      self.temp      = try? values.decode(Double?.self, forKey: .temp     )
      self.feelsLike = try? values.decode(Double?.self, forKey: .feelsLike)
      self.tempMin   = try? values.decode(Double?.self, forKey: .tempMin  )
      self.tempMax   = try? values.decode(Double?.self, forKey: .tempMax  )
      self.pressure  = try? values.decode(Double?.self, forKey: .pressure )
      self.humidity  = try? values.decode(Double?.self, forKey: .humidity )
      self.seaLevel  = try? values.decode(Double?.self, forKey: .seaLevel )
      self.grndLevel = try? values.decode(Double?.self, forKey: .grndLevel)
   }
}
extension DECWeatherData: Hashable {
   
   static func == (lhs: DECWeatherData, rhs: DECWeatherData) -> Bool {
      return (lhs.temp == rhs.temp)
   }
}
