
import Foundation

struct DECWind {
   
   let speed: Double!
   let deg  : Double!
   let gust : Double!
   
   enum CodingKeys: String, CodingKey {
      
      case speed
      case deg
      case gust
   }
}
extension DECWind: Decodable {
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      
      self.speed = try? values.decode(Double?.self, forKey: .speed)
      self.deg   = try? values.decode(Double?.self, forKey: .deg)
      self.gust  = try? values.decode(Double?.self, forKey: .gust)
   }
}

extension DECWind: Hashable {
   
   static func == (lhs: DECWind, rhs: DECWind) -> Bool {
      return (lhs.speed == rhs.speed)
   }
}
