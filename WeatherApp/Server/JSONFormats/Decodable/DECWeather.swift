
import Foundation

struct DECWeather {
   
   let id         : Int?
   let main       : String?
   let description: String?
   let icon       : String?
   let weatherType: WeatherType
   enum CodingKeys: String, CodingKey {
      
      case id
      case main
      case description
      case icon
   }
}
extension DECWeather: Decodable {
   
   init(from decoder: Decoder) throws {
      
      let values = try decoder.container(keyedBy: CodingKeys.self)
      self.id          = try? values.decode(Int?.self,    forKey: .id         )
      self.main        = try? values.decode(String?.self, forKey: .main       )
      self.description = try? values.decode(String?.self, forKey: .description)
      self.icon        = try? values.decode(String?.self, forKey: .icon       )
      self.weatherType = WeatherType(rawValue: self.main ?? "") ?? .non
   }
}

enum WeatherType: String {
   
   case Thunderstorm
   case Drizzle
   case Rain
   case Snow
   case Atmosphere
   case Clear
   case Clouds
   case non
}

extension DECWeather: Hashable {
   
   static func == (lhs: DECWeather, rhs: DECWeather) -> Bool {
      return (lhs.id == rhs.id)
   }
}
