
import Foundation

struct DECCurrentWeather {
   
   let coordinate : DECCoordinate!
   let weathers   : [DECWeather]!
   let base       : String!
   let weatherData: DECWeatherData!
   let visibility : String!
   let wind       : DECWind!
   let clouds     : DECClouds!
   let weatherDate: Date?
   let sunActive  : DECSunActive!
   let timezone   : Int?
   let message    : String!
   let name       : String!
   let cod        : Int!
   
   enum CodingKeys: String, CodingKey {
      case coordinate  = "coord"
      case weather     = "weather"
      case base        = "base"
      case weatherData = "main"
      case visibility  = "visibility"
      case wind        = "wind"
      case clouds      = "clouds"
      case weatherDate = "dt"
      case sunActive   = "sys"
      case timezone    = "timezone"
      case message     = "message"
      case name
      case cod
   }
}
extension DECCurrentWeather: Decodable {
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      
      self.coordinate  = try? values.decode(DECCoordinate?.self,  forKey: .coordinate)
      self.weathers    = try? values.decode([DECWeather]?.self,   forKey: .weather)
      self.base        = try? values.decode(String?.self,         forKey: .base)
      self.weatherData = try? values.decode(DECWeatherData?.self, forKey: .weatherData)
      self.visibility  = try? values.decode(String?.self,         forKey: .visibility)
      self.message     = try? values.decode(String?.self,         forKey: .message)
      self.wind        = try? values.decode(DECWind?.self,        forKey: .wind)
      self.clouds      = try? values.decode(DECClouds?.self,      forKey: .clouds)
      self.sunActive   = try? values.decode(DECSunActive?.self,   forKey: .sunActive)
      self.timezone    = try? values.decode(Int?.self,            forKey: .timezone)
      self.name        = try? values.decode(String?.self,         forKey: .name)
      self.cod         = try? values.decode(Int?.self,            forKey: .cod)
      let timeInterval = (try? values.decode(Int?.self, forKey: .weatherDate)) ?? 0
      self.weatherDate = Date(timeIntervalSince1970: TimeInterval(timeInterval))
   }
}

extension DECCurrentWeather: Hashable {

   func hash(into hasher: inout Hasher) {
      hasher.combine(self.weatherDate?.getDateStaring(format: .weekday))
   }
}

extension DECCurrentWeather: Equatable {
   
   static func == (lhs: DECCurrentWeather, rhs: DECCurrentWeather) -> Bool {
      return lhs.weatherDate?.getDateStaring(format: .weekday) == rhs.weatherDate?.getDateStaring(format: .weekday)
   }
}
