
import Foundation

struct DECForecastWeather {
   
   let message     : String!
   let cod         : Int!
   let listWeathers: [DECCurrentWeather]!
   
   enum CodingKeys: String, CodingKey {
      
      case message
      case cod
      case listWeathers = "list"
   }
}
extension DECForecastWeather: Decodable {
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      self.listWeathers = try? values.decode([DECCurrentWeather]?.self, forKey: .listWeathers)
      self.cod          = try? values.decode(Int?.self,                 forKey: .cod)
      self.message      = try? values.decode(String?.self,              forKey: .message)
   }
}
