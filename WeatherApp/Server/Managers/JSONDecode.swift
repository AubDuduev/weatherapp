
import Foundation

class JSONDecode: Sessionoble {
   
   private let decoder = JSONDecoder()
   
   public func decode<T: Decodable>(jsonType: T.Type , url: URL?, header: Header? = nil, body: Data? = nil, httpMethod: String.HTTPMethod,  completion: @escaping ClosureDecode){
      
      self.session(url: url, body: body, httpMethod: httpMethod, header: header) { (sessionResult) in
         switch sessionResult {
            case .data(let data):
               do {
                  
                  let json = try self.decoder.decode(T.self, from: data)
                  completion(.json(json))
                  let jsonString = String(data: data, encoding: .utf8) ?? " Error JsonString"
                  print("Success parse JSONDecode \(jsonString) string convert")
                  
                  //return error
               } catch let error {
                  let jsonString = String(data: data, encoding: .utf8) ?? " Error JsonString"
                  print(error.localizedDescription, "Error parse JSONDecode \(jsonString) string convert")
                  completion(.error(ErrorServer.ErrorParse))
               }
            case .error(let error):
               print(error!.localizedDescription, "Error JSONDecode get from Server ")
               completion(.error(error))
         }
      }
   }
   public func decode<T: Decodable>(jsonType: T.Type, data: Data, completion: @escaping ClosureDecode){
      
      do {
         
         let json = try self.decoder.decode(T.self, from: data)
         completion(.json(json))
         let jsonString = String(data: data, encoding: .utf8) ?? " Error JsonString"
         print("Success parse JSONDecode \(jsonString) string convert")
         
         //return error
      } catch let error {
         let jsonString = String(data: data, encoding: .utf8) ?? " Error JsonString"
         print(error.localizedDescription, "Error parse JSONDecode \(jsonString) string convert")
         completion(.error(ErrorServer.ErrorParse))
      }
   }
}

