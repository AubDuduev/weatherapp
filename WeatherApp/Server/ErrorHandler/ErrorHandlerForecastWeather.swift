
import UIKit

class ErrorHandlerForecastWeather {
   
   private var errorText: String!
   
   public func check(response: DECForecastWeather?) -> Bool {
      do {
         try self.error(response: response)
      } catch HandlerError.Success {
         return true
      } catch HandlerError.Nil {
         AlertApple.shared.customText(title: .error, message: .message(self.errorText))
         return false
      } catch HandlerError.Error {
         AlertApple.shared.customText(title: .error, message: .message(self.errorText))
         return false
      } catch {
         AlertApple.shared.default(title: .error, message: .errorUnknown)
         return false
      }
      return false
   }
   private func error(response: DECForecastWeather?) throws  {
      
      //Ошибка запроса
      guard let response = response else { throw HandlerError.Nil }
      
      //Текст ошибки
      if let message = response.message {
         self.errorText = message
         throw HandlerError.Error
      }
      
      //Ошибки не обнаружены
      throw HandlerError.Success
   }
   
   private enum HandlerError: String, Error {
      
      case Nil    = "Ошибка запроса"
      case Empty  = "Ошибка "
      case Error
      case Success
   }
}


