
import Foundation

//Получаем список погод 
//api.openweathermap.org/data/2.5/forecast?id=539839&appid=d978a9711a6ba740fc222515d1d99086&units=metric&lang=ru

final class GETForecastWeather: Requestoble, Sessionoble {
   
   private var urlBase    = URLAbstractBase()
   private var parameters = URLParameters()
   private var headers    = URLHeaders()
   private var jsonDecode = JSONDecode()
   private var urlBody    = URLBody()
   private var group      = DispatchGroup()
   private var sessionData: Data!
   
   public func request(data: Any?, completionRequest: @escaping ClosureRequest) {
      
      let parameters = data as! ForecastParameters
      let url        = self.urlBase.create(type: .CurrentForecast(parameters))?.URL
      
      self.group.enter()
      //Получение данных
      session(url: url, httpMethod: .get) { sessionResult in
         switch sessionResult {
            //Success
            case .data(let data):
               self.sessionData = data
               self.group.leave()
            //Error
            case .error(let error):
               print(error!.localizedDescription, "Error JSONDecode get from Server ")
               completionRequest(.error(error))
               self.group.leave()
         }
      }
      self.group.notify(queue: .main) {
         //Декодинг данных
         self.jsonDecode.decode(jsonType: DECForecastWeather.self, data: self.sessionData) { (decodeResult) in
            //Decoding
            switch decodeResult {
               //Error
               case .error(let error):
                  completionRequest(.error(error))
               //Suссes
               case .json(let object):
                  if let forecastWeather = object as? DECForecastWeather {
                     completionRequest(.object(forecastWeather))
                  }
            }
         }
      }
   }
}
