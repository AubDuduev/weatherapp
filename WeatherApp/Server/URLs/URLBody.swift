
import UIKit

class URLBody {
   
   enum Types {
      
      case none
   }
   
   public func create(type: Types?) -> Data? {
      guard let type = type else { return nil }
      
      switch type {
         
         case .none:
            return nil
      }
   }
}
