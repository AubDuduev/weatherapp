//
//  WeatherParameters.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//

import Foundation

struct WeatherParameters {
   
   let lat  : String!
   let lon  : String!
   let lang : String!
   let units: String! = "metric"
}
