
import Foundation

class URLParameters: NSObject {
   
   private var queryKeyQueryValue  : [QueryKey: QueryValue]!
   private var queryKeyStringValue : [QueryKey: String?]!
   private var stringKeyStringValue: [String: String]!
   
   private let messageSMS = "Ваш код для входа в приложение"
   private let apiKey     = "d978a9711a6ba740fc222515d1d99086"
   
   enum QueryItemsType {
      
      case none
      case CurrentWeather(WeatherParameters)
      case CurrentForecast(ForecastParameters)
   }
   
   enum QueryKey: String {
      
      case none
      case latitude  = "lat"
      case longitude = "lon"
      case apiKey    = "appid"
      case language  = "lang"
      case units
      case id
      case count     = "cnt"
   }
   enum QueryValue: String {
      
      case none
   }
   enum QueryValueString {
      
      case none
   }
   enum QueryKeyStringValueString {
      
      case none
   }
   enum DictionaryType {
      
      case QueryKeyQueryValue([QueryKey: QueryValue])
      case QueryKeyStringValue([QueryKey: String?])
      case StringKeyStringValue([String: String])
   }
   
   //MARK: - Функция для создании параметров для URL
   public func create(queryItems: QueryItemsType, _ valueString: QueryValueString = .none, _ queryKeyStringValueString: QueryKeyStringValueString = .none) -> [URLQueryItem]? {
      switch queryItems {
         
         case .none:
            return nil
         //MARK: - Параметр для получения текущей погоды
         //https://api.openweathermap.org/data/2.5/weather?lat=59,5619&lon=30,1850&appid=d978a9711a6ba740fc222515d1d99086&lang=ru
         case .CurrentWeather(let parameters):
            self.queryKeyStringValue = [.apiKey   : self.apiKey,
                                        .latitude : parameters.lat,
                                        .longitude: parameters.lon,
                                        .language : parameters.lang,
                                        .units    : parameters.units]
            return self.createQueryItems(dictionaryType: .QueryKeyStringValue(self.queryKeyStringValue))
         //MARK: - Параметр для получения списка дней погоды
         //https://api.openweathermap.org/data/2.5/forecast?id=539839&appid=d978a9711a6ba740fc222515d1d99086
         case .CurrentForecast(let parameters):
            self.queryKeyStringValue = [.apiKey   : self.apiKey,
                                        .latitude : parameters.lat,
                                        .longitude: parameters.lon,
                                        .language : parameters.lang,
                                        .units    : parameters.units]
            return self.createQueryItems(dictionaryType: .QueryKeyStringValue(self.queryKeyStringValue))
      }
   }
   
   //MARK: - Функция для генерации параметров для URL
   //пробегается с помощью MAP по словарю и выдает тип [URLQueryItem]
   private func createQueryItems(dictionaryType: DictionaryType) -> [URLQueryItem] {
      switch dictionaryType {
         
         //Создаем параметр из статических Key/Value
         case .QueryKeyQueryValue(let dictionary):
            return dictionary.map{ URLQueryItem(name: $0.rawValue, value: $1.rawValue) }
            
         //Создаем параметр из статический Key и динамического Value
         case .QueryKeyStringValue(let dictionary):
            
            let result = dictionary.compactMap { i -> URLQueryItem? in
               if i.value != nil {
                  return URLQueryItem(name: i.key.rawValue, value: i.value)
               } else {
                  return nil
               }
            }
            
            return result
            
         //Создаем параметр из динамического Key и динамического Value
         case .StringKeyStringValue(let dictionary):
            return dictionary.compactMap { URLQueryItem(name: $0, value: $1) }
      }
   }
}

