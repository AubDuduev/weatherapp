//
//  ForecastParameters.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//

import Foundation

struct ForecastParameters {
   
   let count: String!
   let lat  : String!
   let lon  : String!
   let lang : String! = "ru"
   let units: String! = "metric"
}
