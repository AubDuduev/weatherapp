
import Foundation

//https://api.openweathermap.org/data/2.5/weather?lat=59,5619&lon=30,1850&appid=d978a9711a6ba740fc222515d1d99086&lang=ru

class URLCurrentWeather: URLCreatoble {
   
   private var urlPath    = URLPath()
   private var urls       = URLStaticString()
   private var host       = URLHost()
   private var urlScheme  = URLScheme()
   private var custom     = URLCreateCustom()
   private var parameters = URLParameters()
   
   public func url(_ type: URLAbstractFactory.Types) -> ReturnURL? {
      
      switch type {
         case .CurrentWeather(let weatherParameters):
            let queryItems = self.parameters.create(queryItems: .CurrentWeather(weatherParameters))
            let path       = self.urlPath.create(change: .CurrentWeather(.data, .pin, .weather))
            let host       = self.host.create(.Static(.openWeatherMap))
            let customURL  = self.custom.create(type: .init(scheme: .https, host: host, path: path, queryItems: queryItems))
            let url        = URL(string: customURL.string ?? "")
            return (customURL.string, url)
         default:
            return nil
      }
   }
}
