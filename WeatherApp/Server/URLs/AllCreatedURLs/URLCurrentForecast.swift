
import Foundation

//https://api.openweathermap.org/data/2.5/forecast?id=539839&appid=d978a9711a6ba740fc222515d1d99086
class URLCurrentForecast: URLCreatoble {
   
   private var urlPath    = URLPath()
   private var urls       = URLStaticString()
   private var host       = URLHost()
   private var urlScheme  = URLScheme()
   private var custom     = URLCreateCustom()
   private var parameters = URLParameters()
   
   public func url(_ type: URLAbstractFactory.Types) -> ReturnURL? {
      
      switch type {
         case .CurrentForecast(let forecastParameters):
            let queryItems = self.parameters.create(queryItems: .CurrentForecast(forecastParameters))
            let path       = self.urlPath.create(change: .CurrentForecast(.data, .pin, .forecast))
            let host       = self.host.create(.Static(.openWeatherMap))
            let customURL  = self.custom.create(type: .init(scheme: .https, host: host, path: path, queryItems: queryItems))
            let url        = URL(string: customURL.string ?? "")
            return (customURL.string, url)
         default:
            return nil
      }
   }
}
