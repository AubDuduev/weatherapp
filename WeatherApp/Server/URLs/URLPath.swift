
import Foundation

class URLPath {
   
   enum Path: String {
      
      case non = ""
      case data
      case forecast
      case weather
      case hourly
      case pin = "2.5"
   }
   enum ChangePath {
      
      case non
      case CurrentWeather(Path, Path, Path)
      case CurrentForecast(Path, Path, Path)
   }
   
   public func create(change: ChangePath) -> String {
      //create Change Path for url
      switch change {
         //MARK: - Non
         case .non:
            return ""
         //https://api.openweathermap.org/data/2.5/weather
         case .CurrentWeather(let data, let pin, let weather):
            return ["", data.rawValue, pin.rawValue, weather.rawValue].joined(separator: "/")
            
         //https://api.openweathermap.org/data/2.5/forecast
         case .CurrentForecast(let data, let pin, let forecast):
            return ["", data.rawValue, pin.rawValue, forecast.rawValue].joined(separator: "/")
      }
   }
}

