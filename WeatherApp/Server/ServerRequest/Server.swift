
import UIKit

class Server {
   
   //MARK: - Private variable
   private var requestoble : Requestoble!
   private let network     = GDInternetСheck()
   
   //MARK: - Request Server
   public func request(requestoble: Requestoble, data: Any? = nil, completion: @escaping ClosureServerResult){
      
      //check network
      guard self.network.check() else { return }
      
      //request
      requestoble.request(data: data) { (requestResult) in
         //response
         switch requestResult{
            //success
            case .object(let json):
               DispatchQueue.main.async {
                  completion(.object(json))
               }
            //error
            case .error(let error):
               DispatchQueue.main.async {
                  completion(.error(error))
               }
         }
      }
   }
}
