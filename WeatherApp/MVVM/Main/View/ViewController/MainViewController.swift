import UIKit

final class MainViewController: UIViewController {
   
   //MARK: - ViewModel
   lazy public var viewModel = MainViewModel(viewController: self)
    
   //MARK: - Public variable
   public var timer: Timer!
   
   //MARK: - Outlets
   @IBOutlet weak var currentCityLabel           : UILabel!
   @IBOutlet weak var currentTimeLabel           : UILabel!
   @IBOutlet weak var temperatureLabel           : UILabel!
   @IBOutlet weak var descriptionLabel           : UILabel!
   @IBOutlet weak var windLabel                  : UILabel!
   @IBOutlet weak var weatherImageView           : UIImageView!
   @IBOutlet weak var backgroundImageView        : UIImageView!
   @IBOutlet weak var loadingImageView           : UIImageView!
   @IBOutlet weak var cityVisualEffectView       : UIVisualEffectView!
   @IBOutlet weak var temperatureVisualEffectView: UIVisualEffectView!
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModel.viewDidLoad()
   }
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      self.viewModel.viewDidAppear()
   }
}
