
import Foundation

final class LogicMain: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainViewModel!
   
   public func createWeatherParameters() -> WeatherParameters {
      let latitude  = String(UserDataManager.shared.userData.latitude)
      let longitude = String(UserDataManager.shared.userData.longitude)
      let parameters = WeatherParameters(lat: latitude, lon: longitude, lang: "ru")
   return parameters
   }
   public func updateWeatherData(currentWeather: DECCurrentWeather){
      UserDataManager.shared.updateCurrentWeather(currentWeather: currentWeather)
   }
   public func setMyLocation(){
      //Проверяем есть ли интернет
      if GDInternetСheck.check() {
         GDLocationManager.shared.setup()
         GDLocationManager.shared.returnLocationUser = { [weak self] coordinate in
            guard let self = self else { return }
            UserDataManager.shared.updateUserDataLocation(coordinate2D: coordinate)
            self.VM.modelMain = .getCurrentWeather
            GDLocationManager.shared.stopUserLocation()
         }
         GDLocationManager.shared.getUserLocation()
      } else {
         self.VM.managers.presentation.mainData()
      }
   }
   public func testGeoposition() -> Bool {
      if !GDLocationManager.shared.isState() {
         GDLocationManager.shared.authorisation()
         return false
      } else {
         return true
      }
   }
}
//MARK: - Initial
extension LogicMain {
   
   //MARK: - Inition
   convenience init(viewModel: MainViewModel) {
      self.init()
      self.VM = viewModel
   }
}
