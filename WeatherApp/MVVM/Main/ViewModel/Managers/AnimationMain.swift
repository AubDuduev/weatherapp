
import UIKit
import SkeletonView

final class AnimationMain: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainViewModel!
   
   public func animationLoading(isAnimation: Bool){
     
   }
}
//MARK: - Initial
extension AnimationMain {
   
   //MARK: - Inition
   convenience init(viewModel: MainViewModel) {
      self.init()
      self.VM = viewModel
   }
}

