import UIKit

final class ServerMain: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainViewModel!
   
   
   public func getCurrentWeather(parameters: WeatherParameters, completion: @escaping Closure<DECCurrentWeather?>){
      
      //Request
      self.VM.server.request(requestoble: GETCurrentWeather(), data: parameters) { [weak self] (serverResult) in
         guard let self = self else { return }
         //Response
         switch serverResult {
            case .error(let error):
               guard let error = error else { return }
               print("Error server data: class: ServerMain ->, function: getCurrentWeather -> data: DECCurrentWeather ->, description: ", error.localizedDescription)
               AlertApple.shared.default(title: .error, message: .noJSON) { [weak self] in
                  guard let self = self else { return }
                  self.getCurrentWeather(parameters: parameters, completion: completion)
               }
            //Success
            case .object(let object):
               let currentWeather = object as? DECCurrentWeather
               completion(currentWeather)
               print("Successful data: class: ServerMain ->, function: getCurrentWeather ->, data: DECCurrentWeather")
               
         }
      }
   }
}
//MARK: - Initial
extension ServerMain {
   
   //MARK: - Inition
   convenience init(viewModel: MainViewModel) {
      self.init()
      self.VM = viewModel
   }
}


