
import UIKit

final class ConfiguratorMain: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainViewModel!
   
   public func background(){
      self.VM.VC.backgroundImageView.image = UIImage(named: self.VM.dayParts.now().rawValue)
   }
}
//MARK: - Initial
extension ConfiguratorMain {
   
   //MARK: - Inition
   convenience init(viewModel: MainViewModel) {
      self.init()
      self.VM = viewModel
   }
}


