
import UIKit

final class PresentationMain: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainViewModel!
   
   public func mainData(weather: DECCurrentWeather){
      self.VM.VC.currentCityLabel.text  = weather.name
      self.VM.VC.temperatureLabel.text  = String(Int(weather.weatherData.temp ?? 0)) + "℃"
      self.VM.VC.descriptionLabel.text  = weather.weathers?.first?.description
      self.VM.VC.windLabel.text         = "Скорость ветера: \(weather.wind.speed ?? 0) м/с"
      let nameImage = weather.weathers?.first?.weatherType.rawValue ?? ""
      self.VM.VC.weatherImageView.image = UIImage(named: nameImage)
      self.VM.VC.loadingImageView.image = nil
   }
   public func mainData(){
      if let city = UserDataManager.shared.currentWeather.city, !city.isEmpty {
         self.VM.VC.currentCityLabel.text  = UserDataManager.shared.currentWeather.city
         self.VM.VC.temperatureLabel.text  = (UserDataManager.shared.currentWeather.temperature ?? "0") + "℃"
         self.VM.VC.descriptionLabel.text  = UserDataManager.shared.currentWeather.descriptor
         self.VM.VC.windLabel.text         = "Скорость ветера: \(UserDataManager.shared.currentWeather.wind ?? "0") м/с"
         let nameImage = UserDataManager.shared.currentWeather.weatherIcon ?? ""
         self.VM.VC.weatherImageView.image = UIImage(named: nameImage)
         self.VM.VC.loadingImageView.image = nil
      }
   }
   public func time(){
      self.VM.VC.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
         let data = Date()
         self.VM.VC.currentTimeLabel.text = data.getDateStaring(format: .time)
      }
   }
}
//MARK: - Initial
extension PresentationMain {
   
   //MARK: - Inition
   convenience init(viewModel: MainViewModel) {
      self.init()
      self.VM = viewModel
   }
}

