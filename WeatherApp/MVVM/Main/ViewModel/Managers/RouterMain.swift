
import UIKit

final class RouterMain: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainViewModel!
   
   
   public func push(_ type: Push){
      
      switch type {
         case .Map:
            self.pushMapVC()
      }
   }
   
   enum Push {
      case Map
   }
}
//MARK: - Private functions
extension RouterMain {
   
   private func pushMapVC(){
      let mapVC = self.VM.VC.getVCForID(storyboardID     : .Map,
                                        vcID             : .MapVC,
                                        transitionStyle  : .coverVertical,
                                        presentationStyle: .formSheet) as! MapViewController
      //Обновляем данные о погоде 
      mapVC.callback = {
         self.VM.modelMain = .getCurrentWeather
      }
      self.VM.VC.present(mapVC, animated: true)
   }
}
//MARK: - Initial
extension RouterMain {
   
   //MARK: - Inition
   convenience init(viewModel: MainViewModel) {
      self.init()
      self.VM = viewModel
   }
}



