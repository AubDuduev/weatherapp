//
//  ActionsMainViewController.swift
//  WeatherApp
//
//  Created by Senior Developer on 16.07.2021.
//
import UIKit

extension MainViewController {
  
  @IBAction func pushMapButton(button: UIButton){
      self.viewModel.modelMain = .pushMapButton
  }
}
