
import Foundation

final class MainViewModel: VMManagers {
   
   public var modelMain: MainModel? {
      didSet { self.logicMainModel() }
   }
   
   //MARK: - Public variable
   public let dayParts                   = DayParts()
   public let server                     = Server()
   public let errorHandlerCurrentWeather = ErrorHandlerCurrentWeather()
   
   //MARK: - Managers
   lazy public var managers = MainManagers(viewModel: self)
   //MARK: - View controller
   public unowned var VC: MainViewController!
   
   public func viewDidLoad() {
      self.managers.presentation.time()
      self.managers.configurator.background()
   }
   public func viewDidAppear() {
      self.managers.logic.setMyLocation()
   }
   private func logicMainModel(){
      guard let model = self.modelMain else { return }
      
      switch model {
         //MARK: - Server
         case .getCurrentWeather:
            //анимация
            self.managers.animation.animationLoading(isAnimation: true)
            //Запрос на текущую погоду
            let parameters = self.managers.logic.createWeatherParameters()
            self.managers.server.getCurrentWeather(parameters: parameters){ response in
               //Обрабатываем ошибки запроса
               guard self.errorHandlerCurrentWeather.check(response: response) else { return }
               //отображаем данные
               self.managers.presentation.mainData(weather: response!)
               //пересохроняем локально
               self.managers.logic.updateWeatherData(currentWeather: response!)
            }
         //MARK: - Router
         case .pushMapButton:
            guard self.managers.logic.testGeoposition() else { return }
            self.managers.router.push(.Map)
      }
   }
}
//MARK: - Initial
extension MainViewModel {
   
   convenience init(viewController: MainViewController) {
      self.init()
      self.VC = viewController
   }
}
