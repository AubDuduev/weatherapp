
import Foundation

final class MainManagers: VMManagers {
   
   let configurator : ConfiguratorMain!
   let server       : ServerMain!
   let presentation : PresentationMain!
   let logic        : LogicMain!
   let animation    : AnimationMain!
   let router       : RouterMain!
   
   init(viewModel: MainViewModel) {
      
      self.configurator = ConfiguratorMain(viewModel: viewModel)
      self.server       = ServerMain(viewModel: viewModel)
      self.presentation = PresentationMain(viewModel: viewModel)
      self.logic        = LogicMain(viewModel: viewModel)
      self.animation    = AnimationMain(viewModel: viewModel)
      self.router       = RouterMain(viewModel: viewModel)
   }
}

