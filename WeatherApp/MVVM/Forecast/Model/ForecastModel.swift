
import UIKit

enum ForecastModel {
	
   //MARK: - Server
   case getForecastWeather
   //MARK: - Presentation
   case presentationForecastCollectionCell(ForecastCollectionCell)
   //MARK: - Animation
   case animationCell(ForecastCollectionCell, IndexPath, UICollectionView)
}

