
import Foundation

final class ForecastManagers: VMManagers {
   
   let configurator : ConfiguratorForecast!
   let server       : ServerForecast!
   let presentation : PresentationForecast!
   let logic        : LogicForecast!
   let animation    : AnimationForecast!
   let router       : RouterForecast!
   
   init(viewModel: ForecastViewModel) {
      
      self.configurator = ConfiguratorForecast(viewModel: viewModel)
      self.server       = ServerForecast(viewModel: viewModel)
      self.presentation = PresentationForecast(viewModel: viewModel)
      self.logic        = LogicForecast(viewModel: viewModel)
      self.animation    = AnimationForecast(viewModel: viewModel)
      self.router       = RouterForecast(viewModel: viewModel)
   }
}

