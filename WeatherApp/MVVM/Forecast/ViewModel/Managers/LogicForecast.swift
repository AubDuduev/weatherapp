
import Foundation

final class LogicForecast: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: ForecastViewModel!
   
   public func createForecastParameters() -> ForecastParameters {
      let latitude   = String(UserDataManager.shared.userData.latitude)
      let longitude  = String(UserDataManager.shared.userData.longitude)
      let parameters = ForecastParameters(count: "7", lat: latitude, lon: longitude)
      return parameters
   }
   public func sorted(weathers: [DECCurrentWeather]?) -> [DECCurrentWeather]?{
      var weathers = weathers!.compactMap({ $0})
      var weathersSet: Set<DECCurrentWeather> = []
      weathers.forEach{ weathersSet.insert($0) }
      weathers = weathersSet.map({ $0})
      return weathers
   }
   public func removeData(){
      self.VM.VC.forecastCollection.weathers?.removeAll()
      self.VM.VC.forecastCollection.collectionView.reloadData()
   }
}
//MARK: - Initial
extension LogicForecast {
   
   //MARK: - Inition
   convenience init(viewModel: ForecastViewModel) {
      self.init()
      self.VM = viewModel
   }
}
