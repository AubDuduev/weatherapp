import UIKit


final class ServerForecast: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: ForecastViewModel!
   
   public func getForecastWeather(parameters: ForecastParameters, completion: @escaping Closure<DECForecastWeather?>){
      
      //Request
      self.VM.server.request(requestoble: GETForecastWeather(), data: parameters) { [weak self] (serverResult) in
         guard let self = self else { return }
         //Response
         switch serverResult {
            case .error(let error):
               guard let error = error else { return }
               print("Error server data: class: ServerMain ->, function: getCurrentWeather -> data: DECForecastWeather ->, description: ", error.localizedDescription)
               AlertApple.shared.default(title: .error, message: .noJSON) { [weak self] in
                  guard let self = self else { return }
                  self.getForecastWeather(parameters: parameters, completion: completion)
               }
            //Success
            case .object(let object):
               let forecastWeather = object as? DECForecastWeather
               completion(forecastWeather)
               print("Successful data: class: ServerMain ->, function: getCurrentWeather ->, data: DECForecastWeather")
               
         }
      }
   }
}
//MARK: - Initial
extension ServerForecast {
   
   //MARK: - Inition
   convenience init(viewModel: ForecastViewModel) {
      self.init()
      self.VM = viewModel
   }
}


