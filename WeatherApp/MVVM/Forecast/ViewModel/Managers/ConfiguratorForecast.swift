
import UIKit

final class ConfiguratorForecast: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: ForecastViewModel!
   
   
   public func forecastCollection(){
      let collectionViewLayout = ForecastCollectionViewLayout()
      collectionViewLayout.sectionInset            = .init(top: 0, left: 0, bottom: 0, right: 0)
      collectionViewLayout.sectionInsetReference   = .fromContentInset
      collectionViewLayout.minimumLineSpacing      = 8
      collectionViewLayout.minimumInteritemSpacing = 0
      collectionViewLayout.scrollDirection         = .vertical
      self.VM.VC.forecastCollectionView.collectionViewLayout = collectionViewLayout
      self.VM.VC.forecastCollection.collectionView           = self.VM.VC.forecastCollectionView
      self.VM.VC.forecastCollection.viewModel                = self.VM
   }

   public func background(){
      self.VM.VC.backgroundImageView.image = UIImage(named: self.VM.dayParts.now().rawValue)
   }
}
//MARK: - Initial
extension ConfiguratorForecast {
   
   //MARK: - Inition
   convenience init(viewModel: ForecastViewModel) {
      self.init()
      self.VM = viewModel
   }
}


