
import UIKit

final class PresentationForecast: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: ForecastViewModel!
   
   public func forecastCollection(weathers: [DECCurrentWeather]?){
      self.VM.VC.forecastCollection.weathers = weathers
      self.VM.VC.forecastCollectionView.reloadData()
   }
}
//MARK: - ForecastCollectionCell
extension PresentationForecast {
   
   public func presentationForecastCollectionCell(cell: ForecastCollectionCell){
      cell.temperatureLabel.text = String(Int(cell.weather.weatherData.temp ?? 0)) + "℃"
      cell.dateLabel.text        = cell.weather.weatherDate?.getDateStaring(format: .weekday)
      let nameImage = cell.weather.weathers?.first?.weatherType.rawValue ?? ""
      cell.weatherImageView.image = UIImage(named: nameImage)
   }
}
//MARK: - Initial
extension PresentationForecast {
   
   //MARK: - Inition
   convenience init(viewModel: ForecastViewModel) {
      self.init()
      self.VM = viewModel
   }
}

