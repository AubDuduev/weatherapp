
import UIKit

final class AnimationForecast: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: ForecastViewModel!
   
   public func animationCell(cell: ForecastCollectionCell, indexPath: IndexPath, collectionView: UICollectionView){
      cell.transform = CGAffineTransform(translationX: collectionView.bounds.width, y: 0)
      let returnTime = Double("0.\(indexPath.row + 1)")!
      UIView.animate(withDuration: 0.5, delay: returnTime, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .overrideInheritedOptions) {
         cell.transform = .identity
      }
   }
   
}
//MARK: - Initial
extension AnimationForecast {
   
   //MARK: - Inition
   convenience init(viewModel: ForecastViewModel) {
      self.init()
      self.VM = viewModel
   }
}

