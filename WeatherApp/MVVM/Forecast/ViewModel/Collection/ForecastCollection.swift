
import UIKit

final class ForecastCollection : NSObject {
   
   //MARK: - Variable
   public var collectionView: UICollectionView!
   public var viewModel     : ForecastViewModel!
   public var weathers      : [DECCurrentWeather]!
}

//MARK: - Delegate CollectionView
extension ForecastCollection: UICollectionViewDelegate {
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
   }
}

//MARK: - DataSource CollectionView
extension ForecastCollection: UICollectionViewDataSource {
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      self.collectionView = collectionView
      return weathers?.count ?? 0
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      //создание ячейки
      let cell = ForecastCollectionCell().collectionCell(collectionView, indexPath: indexPath)
      cell.configure(viewModel: self.viewModel, weather: self.weathers?[indexPath.row])
      //анимация ячейки
      self.viewModel.modelForecast = .animationCell(cell, indexPath, collectionView)
      return cell
   }
}
//MARK: - DelegateFlowLayout  CollectionView
extension ForecastCollection: UICollectionViewDelegateFlowLayout {
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let width : CGFloat = collectionView.bounds.width
      let height: CGFloat = 70
      return .init(width: width, height: height)
   }
   
}


