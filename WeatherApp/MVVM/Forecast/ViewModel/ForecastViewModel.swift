
import Foundation

final class ForecastViewModel: VMManagers {
   
   public var modelForecast: ForecastModel? {
      didSet { self.logicForecastModel() }
   }
   
   //MARK: - Public variable
   public let dayParts                    = DayParts()
   public let server                      = Server()
   public let errorHandlerForecastWeather = ErrorHandlerForecastWeather()
   
   //MARK: - Managers
   lazy public var managers = ForecastManagers(viewModel: self)
   //MARK: - View controller
   public unowned var VC: ForecastViewController!
   
   public func viewDidLoad() {
      self.managers.configurator.forecastCollection()
      self.managers.configurator.background()
   }
   public func viewWillAppear() {
      self.modelForecast = .getForecastWeather
   }
   public func viewWillDisappear(){
      self.managers.logic.removeData()
   }
   private func logicForecastModel(){
      guard let model = self.modelForecast else { return }
      
      switch model {
         //MARK: - Server
         case .getForecastWeather:
            self.VC.activityIndicator.show(true)
            //Запрос на текущую погоду
            let parameters = self.managers.logic.createForecastParameters()
            self.managers.server.getForecastWeather(parameters: parameters){ [weak self] response in
               guard let self = self else { return }
               //Обрабатываем ошибки запроса
               guard self.errorHandlerForecastWeather.check(response: response) else { return }
               //сортируем только
               let listWeathers = self.managers.logic.sorted(weathers: response?.listWeathers)
               //презентуешь данные
               self.managers.presentation.forecastCollection(weathers: listWeathers)
               //
               self.VC.activityIndicator.show(false)
            }
         //MARK: - Presentation
         case .presentationForecastCollectionCell(let cell):
            self.managers.presentation.presentationForecastCollectionCell(cell: cell)
         //MARK: - Animation
         case .animationCell(let cell, let indexPath, let collectionView):
            self.managers.animation.animationCell(cell: cell, indexPath: indexPath, collectionView: collectionView)
      }
   }
}
//MARK: - Initial
extension ForecastViewModel {
   
   convenience init(viewController: ForecastViewController) {
      self.init()
      self.VC = viewController
   }
}
