import UIKit

final class ForecastViewController: UIViewController {
   
   //MARK: - ViewModel
   lazy public var viewModel = ForecastViewModel(viewController: self)
    
   //MARK: - Public variable
   
   
   //MARK: - Outlets
   @IBOutlet weak var forecastCollection    : ForecastCollection!
   @IBOutlet weak var forecastCollectionView: UICollectionView!
   @IBOutlet weak var backgroundImageView   : UIImageView!
   @IBOutlet weak var activityIndicator     : UIActivityIndicatorView!
   
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModel.viewDidLoad()
   }
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.viewModel.viewWillAppear()
   }
   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      self.viewModel.viewWillDisappear()
   }
}
