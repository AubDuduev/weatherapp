
import UIKit

final class ForecastCollectionCell: UICollectionViewCell, LoadNidoble {
   
   
   private var viewModel: ForecastViewModel!
   
   public var weather: DECCurrentWeather!
   
   @IBOutlet weak var commonView      : UIView!
   @IBOutlet weak var temperatureLabel: UILabel!
   @IBOutlet weak var dateLabel       : UILabel!
   @IBOutlet weak var weatherImageView: UIImageView!
   
   public func configure(viewModel: ForecastViewModel, weather: DECCurrentWeather?){
      self.viewModel = viewModel
      self.weather   = weather
      self.viewModel.modelForecast = .presentationForecastCollectionCell(self)
   }
   
   override func layoutSubviews() {
      super.layoutSubviews()
      self.commonView.cornerRadius(8, true)
   }
}
