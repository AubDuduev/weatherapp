
import UIKit
import CoreLocation
import MapKit

final class MapViewController: UIViewController {
   
   //MARK: - ViewModel
   lazy public var viewModel = MapViewModel(viewController: self)
   
   //MARK: - Public variable
   public var locationManager = CLLocationManager()
   public let server          = Server()
   public var setCoordinate   : CLLocationCoordinate2D!
   public var callback        : ClosureEmpty!
   
   //MARK: - Outlets
   @IBOutlet weak var currentAddressLabel: UILabel!
   @IBOutlet weak var centerPinImageView : UIImageView!
   @IBOutlet weak var mapView            : MKMapView!
   @IBOutlet weak var myLocationButton   : UIButton!
   @IBOutlet weak var myLocationView     : UIView!
   @IBOutlet weak var activityIndicator  : UIActivityIndicatorView!
   
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModel.viewDidLoad()
   }
}
