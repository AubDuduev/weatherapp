//
//  ActionsMapViewController.swift
//  EatRepeat
//
//  Created by Senior Developer on 10.07.2021.
//
import UIKit

extension MapViewController {
   
   @IBAction func backButton(button: UIButton){
      self.viewModel.mapModel = .dismiss
   }
   @IBAction func myLocationButton(button: UIButton){
      self.viewModel.mapModel = .myLocation
   }
}
