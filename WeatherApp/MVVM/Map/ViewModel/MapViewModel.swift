
import Foundation
import SwiftUI
import Combine


final class MapViewModel: VMManagers, ObservableObject {
   
   public var mapModel: MapModel? {
      didSet { self.commonLogicMap() }
   }
   
   //MARK: - Managers
   lazy public var managers = MapManagers(viewModel: self)
   //MARK: - View controller
   public unowned var VC: MapViewController!
   
   public func viewDidLoad() {
      self.managers.configurator.locationManager()
      self.managers.configurator.mapView()
      self.managers.configurator.location()
      self.managers.configurator.myLocationView()
      self.managers.configurator.activityIndicator()
      self.managers.logic.setMyLocation()
   }
   
   public func commonLogicMap(){
      guard let model = self.mapModel else { return }
      
      switch model {
         //MARK: - Logic
         //Переключения на свою геопозицию
         case .myLocation:
            self.managers.logic.setMyLocation()
         //Сохранение своих данных в кор дату
         case .updateUserData(let coordinate2D):
            self.managers.logic.saveUserDataGeolocationLast(coordinate2D: coordinate2D)
         //Возврат на экран погода
         case .dismiss:
            self.managers.logic.dismiss()
         //MARK: - Presentation
         case .currentAddress(let coordinate2D):
            self.managers.logic.loadingGetPosition(isSuccess: false)
            DelayAfter.shared.time(seconds: 1) { [weak self] in
               guard let self = self else { return }
               self.managers.presentation.currentLocation(coordinate2D: coordinate2D)
               self.managers.logic.loadingGetPosition(isSuccess: true)
            }
      }
      
   }
}
//MARK: - Initial
extension MapViewModel {
   
   convenience init(viewController: MapViewController) {
      self.init()
      self.VC       = viewController
      self.managers = MapManagers(viewModel: self)
   }
}
