
import UIKit
import CoreLocation

class PresentationMap: VMManager {
  
  //MARK: - Public variable
   unowned public var VM: MapViewModel!
  
   public func currentLocation(coordinate2D:  CLLocationCoordinate2D){
      let location = CLLocation(latitude: coordinate2D.latitude, longitude: coordinate2D.longitude)
      location.fetchAddress { [weak self]  address, error in
         guard let self = self else { return }
         self.VM.VC.currentAddressLabel.text = address
         guard let error = error else { return }
         print("error decoder address", error.localizedDescription)
      }
   }
}
//MARK: - Initial
extension PresentationMap {
  
  //MARK: - Inition
  convenience init(viewModel: MapViewModel) {
    self.init()
    self.VM = viewModel
  }
}

