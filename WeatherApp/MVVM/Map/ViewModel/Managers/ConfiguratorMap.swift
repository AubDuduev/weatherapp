
import UIKit
import MapKit


class ConfiguratorMap: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MapViewModel!
   
   public func locationManager(){
      self.VM.VC.locationManager.startUpdatingLocation()
   }
   public func mapView(){
      self.VM.VC.mapView.delegate = self.VM.VC
   }
   public func location(){
      GDLocationManager.shared.authorisation()
   }
   public func myLocationView(){
      self.VM.VC.myLocationView.cornerRadius(30, false)
      self.VM.VC.myLocationButton.cornerRadius(30, true)
      self.VM.VC.myLocationView.shadowColor(color: .darkGray, radius: 20, opacity: 0.8)
   }
   
   public func activityIndicator(){
      self.VM.VC.activityIndicator.show(false)
   }
}

//MARK: - Initial
extension ConfiguratorMap {
   
   //MARK: - Inition
   convenience init(viewModel: MapViewModel) {
      self.init()
      self.VM = viewModel
   }
}


