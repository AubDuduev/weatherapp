
import UIKit
import SwiftUI

class RouterMap: VMManager {
   
   //MARK: - Public variable
   unowned public var VM: MapViewModel!
   
}

//MARK: - Initial
extension RouterMap {
   
   //MARK: - Inition
   convenience init(viewModel: MapViewModel) {
      self.init()
      self.VM = viewModel
   }
}



