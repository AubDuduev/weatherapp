import Foundation
import CoreLocation
import MapKit

class LogicMap: VMManager {
  
  //MARK: - Public variable
  unowned public var VM: MapViewModel!
  
   public func saveUserDataGeolocationLast(coordinate2D:  CLLocationCoordinate2D){
      UserDataManager.shared.updateUserDataLocation(coordinate2D: coordinate2D)
   }
   public func dismiss(){
      self.VM.VC.dismiss(animated: true) { [weak self] in
         guard let self = self else { return }
         self.VM.VC.callback?()
      }
   }
   public func setMyLocation(){
      guard GDInternetСheck.check() else { return }
      GDLocationManager.shared.setup()
      GDLocationManager.shared.returnLocationUser = { [weak self] coordinate in
         guard let self = self else { return }
         let eyeAltitude  = CLLocationDistance("1000.0")!
         let camera       = MKMapCamera(lookingAtCenter  : coordinate,
                                        fromEyeCoordinate: coordinate,
                                        eyeAltitude      : eyeAltitude)
         self.VM.VC.mapView.camera = camera
         GDLocationManager.shared.stopUserLocation()
         self.VM.mapModel = .updateUserData(coordinate)
      }
      GDLocationManager.shared.getUserLocation()
   }
   public func loadingGetPosition(isSuccess: Bool){
      
      switch isSuccess {
         case true:
            self.VM.VC.myLocationButton.alpha = 1
            self.VM.VC.activityIndicator.show(false)
         case false:
            self.VM.VC.myLocationButton.alpha = 0.5
            self.VM.VC.activityIndicator.show(true)
      }
   }
}
//MARK: - Initial
extension LogicMap {
  
  //MARK: - Inition
  convenience init(viewModel: MapViewModel) {
    self.init()
    self.VM = viewModel
  }
}
