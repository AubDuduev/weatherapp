import UIKit

class AnimationMap: VMManager {
  
  //MARK: - Public variable
   unowned public var VM: MapViewModel!
  
}
//MARK: - Initial
extension AnimationMap {
  
  //MARK: - Inition
  convenience init(viewModel: MapViewModel) {
    self.init()
    self.VM = viewModel
  }
}

