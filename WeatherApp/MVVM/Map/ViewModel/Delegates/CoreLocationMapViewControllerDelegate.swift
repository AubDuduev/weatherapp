//
//  CoreLocationMapViewControllerDelegate.swift
//  Leap
//
//  Created by Senior Developer on 10.10.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import CoreLocation
import Foundation
import MapKit

extension MapViewController:  MKMapViewDelegate {
   
   func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "test")
      return annotationView
   }
   func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
      self.viewModel.mapModel = .currentAddress(mapView.camera.centerCoordinate)
      self.viewModel.mapModel = .updateUserData(mapView.camera.centerCoordinate)
   }
   
}
