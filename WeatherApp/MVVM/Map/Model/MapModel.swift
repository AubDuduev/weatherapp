
import UIKit

import CoreLocation

enum MapModel {
	
   //MARK: - Logic
   case myLocation
   case dismiss
   //MARK: - Presentation
   case currentAddress(CLLocationCoordinate2D)
   case updateUserData(CLLocationCoordinate2D)
}

