
import Foundation

final class MapManagers: VMManagers {
  
  let configurator: ConfiguratorMap!
  let server      : ServerMap!
  let presentation: PresentationMap!
  let logic       : LogicMap!
  let animation   : AnimationMap!
  let router      : RouterMap!
  
  init(viewModel: MapViewModel) {
    
    self.configurator = ConfiguratorMap(viewModel: viewModel)
    self.server       = ServerMap(viewModel: viewModel)
    self.presentation = PresentationMap(viewModel: viewModel)
    self.logic        = LogicMap(viewModel: viewModel)
    self.animation    = AnimationMap(viewModel: viewModel)
    self.router       = RouterMap(viewModel: viewModel)
  }
}

