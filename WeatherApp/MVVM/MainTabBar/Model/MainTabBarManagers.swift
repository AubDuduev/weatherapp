
import Foundation

final class MainTabBarManagers: VMManagers {
   
   let configurator : ConfiguratorMainTabBar!
   let server       : ServerMainTabBar!
   let presentation : PresentationMainTabBar!
   let logic        : LogicMainTabBar!
   let animation    : AnimationMainTabBar!
   let router       : RouterMainTabBar!
   
   init(viewModel: MainTabBarViewModel) {
      
      self.configurator = ConfiguratorMainTabBar(viewModel: viewModel)
      self.server       = ServerMainTabBar(viewModel: viewModel)
      self.presentation = PresentationMainTabBar(viewModel: viewModel)
      self.logic        = LogicMainTabBar(viewModel: viewModel)
      self.animation    = AnimationMainTabBar(viewModel: viewModel)
      self.router       = RouterMainTabBar(viewModel: viewModel)
   }
}

