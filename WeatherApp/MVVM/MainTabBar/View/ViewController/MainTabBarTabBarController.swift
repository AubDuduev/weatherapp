
import UIKit

final class MainTabBarViewController: UITabBarController {
   
   //MARK: - ViewModel
   lazy public var viewModal = MainTabBarViewModel(tabBarViewController: self)
   
   //MARK: - Public variable
   public let tabBarView = TabBarView().loadNib()
   
   
   //MARK: - Outlets
   
   
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModal.viewDidLoad()
   }
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      self.viewModal.viewDidLayoutSubviews()
   }
}
