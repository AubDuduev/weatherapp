
import Foundation

final class MainTabBarViewModel: VMManagers {
   
   public var modalMainTabBar: MainTabBarViewModel? {
      didSet { self.logicMainTabBarModel() }
   }
   
   //MARK: - Managers
   lazy public var managers = MainTabBarManagers(viewModel: self)
   //MARK: - View controller
   public unowned var tabBarVC: MainTabBarViewController!
   
   public func viewDidLoad() {
      self.managers.configurator.appleTabBar()
      self.managers.configurator.setControllers()
      self.managers.configurator.addedAllView()
      self.managers.configurator.tabBarViewButton()
   }
   public func viewDidLayoutSubviews() {
      self.managers.configurator.tabBarConstraints()
   }
   public func logicMainTabBarModel(){
      //guard let model = self.modalMainTabBar else { return }
      
   }
}
//MARK: - Initial
extension MainTabBarViewModel {
   
   convenience init(tabBarViewController: MainTabBarViewController) {
      self.init()
      self.tabBarVC = tabBarViewController
   }
}
