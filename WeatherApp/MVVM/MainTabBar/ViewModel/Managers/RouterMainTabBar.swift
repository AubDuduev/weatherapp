
import UIKit

final class RouterMainTabBar: VMManager {
  
  //MARK: - Public variable
   public unowned var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension RouterMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}



