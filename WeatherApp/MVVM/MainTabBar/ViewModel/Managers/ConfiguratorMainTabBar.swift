
import UIKit

final class ConfiguratorMainTabBar: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainTabBarViewModel!
   
   public func setControllers(){
      self.VM.tabBarVC.setViewControllers(self.VM.managers.logic.createVcForTabBar(), animated: true)
   }
   public func appleTabBar(){
      self.VM.tabBarVC.tabBar.backgroundColor = .clear
   }
   public func tabBarViewButton(){
      self.VM.managers.logic.tapButton()
   }
   public func addedAllView(){
      self.VM.tabBarVC.tabBar.addSubview(self.VM.tabBarVC.tabBarView)
   }
   
   public func tabBarConstraints(){
      self.VM.tabBarVC.tabBar.bounds.size.height = 80
      self.VM.tabBarVC.tabBarView.snp.makeConstraints { tabBarView in
         tabBarView.edges.equalToSuperview()
      }
      self.VM.tabBarVC.tabBarView.setup(bottomPadding: self.VM.tabBarVC.view.safeAreaInsets.bottom)
   }
}
//MARK: - Initial
extension ConfiguratorMainTabBar {
   
   //MARK: - Inition
   convenience init(viewModel: MainTabBarViewModel) {
      self.init()
      self.VM = viewModel
   }
}


