
import UIKit

final class PresentationMainTabBar: VMManager {
  
  //MARK: - Public variable
   public unowned var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension PresentationMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}

