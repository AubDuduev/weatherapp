import UIKit

final class LogicMainTabBar: VMManager {
   
   //MARK: - Public variable
   public unowned var VM: MainTabBarViewModel!
   
   //MARK: - Create View Controllers For TabBar
   public func createVcForTabBar() -> [UIViewController]{
      let main     = UIStoryboard.createVCID(sbID: .Main, vcID: .MainVC)
      let forecast = UIStoryboard.createVCID(sbID: .Forecast, vcID: .ForecastVC)
      let controllers = [main, forecast]
      return controllers
   }
   public func tapButton() {
      self.VM.tabBarVC.tabBarView.actions = { [weak self] index in
         guard let self = self else { return }
         self.VM.tabBarVC.selectedIndex = index
         self.changesWhenSwitchingTabBar()
         GDFeedbackGenerator.shared.vibration()
      }
   }
   public func changesWhenSwitchingTabBar(){
      
   }
}
//MARK: - Initial
extension LogicMainTabBar {
   
   //MARK: - Inition
   convenience init(viewModel: MainTabBarViewModel){
   self.init()
   self.VM = viewModel
   }
}
