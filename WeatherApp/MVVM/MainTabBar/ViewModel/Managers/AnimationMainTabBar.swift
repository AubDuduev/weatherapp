import UIKit

final class AnimationMainTabBar: VMManager {
  
  //MARK: - Public variable
   public unowned var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension AnimationMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}

