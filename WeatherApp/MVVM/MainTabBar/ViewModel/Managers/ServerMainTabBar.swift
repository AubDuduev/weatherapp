import UIKit

final class ServerMainTabBar: VMManager {
  
  //MARK: - Public variable
   public unowned var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension ServerMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel){
    self.init()
    self.VM = viewModel
  }
}


